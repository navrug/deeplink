# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 15:12:13 2016

@author: Ronan
"""

from sklearn import preprocessing
import numpy as np
import csv
import re

from data_analysis import (special_cases, and_repairing, parenthesis_removal,
                           accent_removal)

"""
This script should create files used in main.lua
"""
#%% Binarizing
def transform(x, lb, all_authors):
    if len(x) == 0:
        return np.zeros(len(all_authors))
    else:
        return np.sum(lb.transform(x), axis = 0)
        
def binarize(authors_by_paper, all_authors):
    lb = preprocessing.LabelBinarizer()
    lb.fit(list(all_authors))
        
    bin_authors = [transform(x, lb, all_authors) for x in authors_by_paper]
    bin_authors = np.array(bin_authors)
    
    return bin_authors    
    
#%%

training_edges = np.loadtxt("training_set.txt", dtype=int)

with open("node_information.csv", "r") as f:
    reader = csv.reader(f)
    node_info  = list(reader)
    
author_caption = [element[3] for element in node_info]
authors_by_paper = []
all_authors = set()
for paper_authors in author_caption:
    s = special_cases(paper_authors)
    s = and_repairing(s)
    s = parenthesis_removal(s)
    s = accent_removal(s)
    s = re.sub(r'~', r' ', s) # Espace insécable
    s = re.sub(r'--', r'-', s)
    s = re.sub(r'_', r'-', s) # cf. Chang_Pu Sun
    # Wrong separators
    s = re.sub(r';', r',', s)
    s = re.sub(r'&', r',', s)
    author_set = s.split(",")
    trimmed_set = []
    for i, author in enumerate(author_set):
        trimmed = author.strip()
        if trimmed != '':
             # Don't use last name when pure initials or single word
            if (i != len(author_set)-1) or \
               ((re.search(r'\w\w', trimmed) is not None) and not \
                (re.search(r' ', trimmed) is None and \
                 re.search(r'\.', trimmed) is None)):
                trimmed = re.sub(r'\.-', r'-', trimmed)
                trimmed = re.sub(r'\.', r' ', trimmed)
                trimmed = re.sub(r'-', r' ', trimmed)
                trimmed = re.sub(r'  ', r' ', trimmed)
                trimmed = trimmed.lower().strip()
                trimmed_set.append(trimmed)
                all_authors.add(trimmed)
    authors_by_paper.append(trimmed_set)

#### The binarized .csv is about 7Go
#binerized_authors_by_paper = binarize(authors_by_paper, all_authors)
#np.savetxt('binarized_authors_by_paper.csv', 
#           binerized_authors_by_paper,
#           delimiter=",")
