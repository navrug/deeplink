# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 19:38:08 2016

@author: gurvan
"""


import csv
import numpy as np
import timeit
import sys
import matplotlib.pyplot as plt

from sklearn.cross_validation import KFold
from sklearn.ensemble import RandomForestClassifier, ExtraTreesClassifier, GradientBoostingClassifier
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression

from feature_engineering import get_edge_features
from util import f_score, anti_f_score

training_edges = np.loadtxt("training_set.txt", dtype=int)
testing_edges = np.loadtxt("testing_set.txt", dtype=int)

with open("node_information.csv", "r") as f:
    reader = csv.reader(f)
    node_info  = list(reader)
    
print "Done importing data."

#%% Bonus edges!
#The test set gives information: half of the pairs are edges, which is much
#better than taking a pair at random.
#We use this information by adding the edges of the test set in which we are
#very confident to the graph, which then helps making better predictions for 
#other pairs.
#"prob_test.npy" should contain the class probabilities given by a model
#trained without bonus edges.

prob_test = np.load("prob_test.npy")
y_pred_test = np.max(prob_test, axis=1).astype(int)
used_prob = prob_test[range(len(prob_test)), y_pred_test]
bonus_idx = y_pred_test.astype(bool) & (used_prob > 0.99)
bonus_edges = [(src, dst, True) for (src, dst) in testing_edges[bonus_idx]]

X = get_edge_features(node_info, training_edges, bonus_edges=bonus_edges)
y = np.array([exists for (src, dst, exists) in training_edges])

#%% Cross-validation


feature_mask = np.ones(len(X[0,:]), dtype=bool)
#feature_mask = np.zeros(len(X[0,:]), dtype=bool)
#feature_mask[-8:] = False
#feature_mask[[6,17,18,20,27,19,28,32,31,29,30,1,2]]= False
#feature_mask[[6,17,18,20]]= False
#%%

n_samples = len(training_edges)

n_folds = 3
kf = KFold(n_samples, n_folds=n_folds)

models = [
    RandomForestClassifier(n_estimators=100, criterion='gini', max_depth=None,
                           min_samples_split=2, min_samples_leaf=1,
                           min_weight_fraction_leaf=0.0, max_features='sqrt',
                           max_leaf_nodes=None, n_jobs=-1, verbose=0,class_weight='balanced'),
#    RandomForestClassifier(n_estimators=100, criterion='entropy', max_depth=None,
#                           min_samples_split=2, min_samples_leaf=1,
#                           min_weight_fraction_leaf=0.0, max_features='sqrt',
#                           max_leaf_nodes=None, n_jobs=-1, verbose=0),
#    ExtraTreesClassifier(n_estimators=100, criterion='gini', max_depth=None,
#                         min_samples_split=2, min_samples_leaf=1,
#                         min_weight_fraction_leaf=0.0, max_features='auto',
#                         max_leaf_nodes=None, bootstrap=False, n_jobs=-1,
#                         verbose=0),
# GradientBoostingClassifier(loss='deviance', learning_rate=0.1,
#                            n_estimators=100, subsample=1.0,
#                            min_samples_split=2, min_samples_leaf=1,
#                            min_weight_fraction_leaf=0.0, max_depth=3,
#                            max_features=None, verbose=0,
#                            max_leaf_nodes=None),
#    LinearSVC(penalty='l2', loss='hinge', dual=True, tol=0.0001, C=2**-2,
#              fit_intercept=True, intercept_scaling=1,  verbose=1, max_iter=1000),
#    LogisticRegression(penalty='l2', dual=False, tol=0.0001, C=2e10,
#                       solver='liblinear', max_iter=100, verbose=1, n_jobs=-1)
          ]
names = ["Gini", "Entropy", "ET", "GB", "LinSVC", "LogReg"]
times = np.zeros(len(models))
accuracy = np.zeros((len(models), n_folds))
f_scores = np.zeros((len(models), n_folds))
anti_f_scores = np.zeros((len(models), n_folds))

start_time = timeit.default_timer()

Xs = X-np.mean(X, axis=0)
Xs /= np.std(Xs, axis=0)

predictions = []
probas  = []

for k, (model, name) in enumerate(zip(models, names)):
    y_pred = np.zeros(len(y), dtype=int)
    prob = np.zeros(len(y))
    for fold, (train, test) in enumerate(kf):
        it_time = timeit.default_timer()
        model.fit(Xs[train][:,feature_mask], y[train])
        y_pred[test] = model.predict(Xs[test][:,feature_mask])
        prob[test] = model.predict_proba(Xs[test][:,feature_mask])[:,1]
        accuracy[k, fold] = np.mean(y[test] == y_pred[test])
        f_scores[k, fold] = f_score(y[test], y_pred[test])
        anti_f_scores[k, fold] = anti_f_score(y[test], y_pred[test])
        duration = ((timeit.default_timer()-it_time) / 60.)
        times[k] += duration
        sys.stdout.write(name + "\racc=%f, f_score=%f, anti_f=%f in %f min.  \n" % (accuracy[k, fold], f_scores[k, fold], anti_f_scores[k, fold], duration))

    predictions.append(y_pred)
    probas.append(prob)
print "\rTotal time:",  ((timeit.default_timer()-start_time) / 60.), "min."


#%% Prediction analysis

used_prob = prob*y_pred + (1-prob)*(1-y_pred)
fails = y != y_pred


plt.figure()
plt.title("Estimated probability of prediction (in log)")
plt.hist(used_prob[y.astype(bool)], alpha=0.5, log=True,bins=20, label='True')
plt.hist(used_prob[(1-y).astype(bool)], alpha=0.5, log=True,bins=20, label='False')
plt.legend(loc='upper left')
plt.figure()

plt.figure()
plt.title("Estimated probability of prediction on failures")
plt.hist(used_prob[fails & y.astype(bool)], alpha=0.5, bins=20, label='True')
plt.hist(used_prob[fails & (1-y).astype(bool)], alpha=0.5, bins=20, label='False')
plt.legend(loc='upper left')

#%% Make submission
make_submission = False
if make_submission:
    X_train, X_test = get_edge_features(node_info, training_edges, bonus_edges=bonus_edges, testing_edges=testing_edges)
    y_train = np.array([exists for (src, dst, exists) in training_edges])
    models[0].fit(X_train[:,feature_mask], y_train)
    y_pred = models[0].predict(X_test[:,feature_mask])
#    y_pred = 1 - y_pred
    sub = np.arange(len(y_pred))
    sub = np.vstack([sub, y_pred])
    sub = sub.T
    np.savetxt("fourth_try.csv", sub, fmt='%i', delimiter=",", header="id,category", comments="")
    
#%%
"""
The following plots the importance of each features in a random forest.
"""
forest = models[0]

importances = forest.feature_importances_
std = np.std([tree.feature_importances_ for tree in forest.estimators_],
             axis=0)
argsort = np.argsort(importances)[::-1]
indices = np.arange(len(feature_mask))[feature_mask][argsort]

# Print the feature ranking
print("Feature ranking:")

for f in range(sum(feature_mask)):
    print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

plt.figure()
plt.title("Feature importances")
plt.bar(range(sum(feature_mask)), importances[indices], color="r", 
        yerr=std[indices], align="center")
plt.xticks(range(sum(feature_mask)), indices)
plt.xlim([-1, sum(feature_mask)])
plt.show()

