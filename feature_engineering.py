# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 15:06:45 2016

@author: gurvan
"""

import nltk
import numpy as np
import re
import sys
import networkx as nx
import string
from collections import Counter
import operator

from util import pref_attachment, cosine, jaccard, shortest_path_length,terms_to_graph, core_dec

nltk.download('stopwords')


#%% Titles

def title_features(node_info):
    stpwds = set(nltk.corpus.stopwords.words("english"))
    stemmer = nltk.PorterStemmer()
    features = []
    titles = [element[2] for element in node_info]
    for title in titles:
        title = title.lower().split(" ")
        title = [token for token in title if token not in stpwds]
        title = [stemmer.stem(token) for token in title]
        features.append(title)
    return features
   
    

#%% Abstracts

def abstract_features(node_info):
    stpwds = set(nltk.corpus.stopwords.words("english"))
    stemmer = nltk.PorterStemmer()
    features = []
    abstracts = [element[3] for element in node_info]
    for abstract in abstracts:
        abstract = abstract.lower().split(" ")
        abstract = [token for token in abstract if token not in stpwds]
        abstract = [stemmer.stem(token) for token in abstract]
        features.append(abstract)
    return features
   
    
#%% Abstract Keywords
    
def abstract_keywords(node_info, mode="PageRank"):
    clean_abs = clean_abstracts(node_info)
    graphs = gow(clean_abs)
    
    if (mode == "PageRank"):
        keywords = pageRank_keywords(graphs)
    elif (mode == "MainCoreWeight"):
        mc_w, mc_u = core_keywords (graphs)
        keywords = mc_w
    elif (mode == "MainCoreUnweight"):
        mc_w, mc_u = core_keywords (graphs)
        keywords = mc_u
    return keywords
    
 
## POS-tag is commented cause it's incredibly slow (it shouldnt... ?) 
def clean_abstracts(node_info):
    stpwds = set(nltk.corpus.stopwords.words("english"))
    punct = string.punctuation.replace("-", "")
    stemmer = nltk.stem.PorterStemmer()
    ##################################
    # read and pre-process abstracts #
    ##################################
    clean_abs = []
    abstracts = [element[5] for element in node_info]
    counter = 0
    for abstract in abstracts:
        # remove formatting
       abstract =  re.sub("\s+", " ", abstract)
       # convert to lower case
       abstract = abstract.lower()
       # remove punctuation (preserving intra-word dashes)
       abstract = "".join(letter for letter in abstract if letter not in punct)
       # remove dashes attached to words but that are not intra-word
       abstract = re.sub("[^[:alnum:]['-]", " ", abstract)
       abstract = re.sub("[^[:alnum:][-']", " ", abstract)
       # remove extra white space
       abstract = re.sub(" +"," ", abstract)
       # remove leading and trailing white space
       abstract = abstract.strip()
       # tokenize
       tokens = abstract.split(" ")
       # remove stopwords
       tokens_keep = [token for token in tokens if token not in stpwds]
       # POS-tag 
#       tagged_tokens = nltk.pos_tag(tokens_keep)
#       # keep only nouns and adjectives    
#       tokens_keep = [pair[0] for pair in tagged_tokens if (pair[1] in ["NN","NNS","NNP","NNPS","JJ","JJS","JJR"])]
#       # apply Porter stemmer
       tokens_keep = [stemmer.stem(token) for token in tokens_keep]
#       # store list of tokens
       if(len(tokens_keep)>5):
           clean_abs.append(tokens_keep)
       else :
           clean_abs.append(['this', 'is', 'a', 'random', 'long', 'abstract','so','that','gow','method','doesnt', 'fail'])
       counter += 1
       if counter % 5000 == 0:
 		print counter, 'abstracts have been processed'
    return clean_abs
    
def gow(clean_abs):
    print "creating a gow for each abstract \n"
    all_graphs = []
    window = 3
    counter = 1
    for abstract in clean_abs:
        if counter % 5000 == 0:
            print counter, 'graphs of words have been created'
        all_graphs.append(terms_to_graph(abstract, window))
        counter += 1
    return all_graphs
       
def core_keywords(graphs):
    print "extracting main cores from each graph \n"
    mcs_weighted = []
    mcs_unweighted = []
    counter = 0
    for graph in graphs:
        mcs_weighted.append(core_dec(graph, weighted = True)["main_core"].vs["name"])
        mcs_unweighted.append(core_dec(graph, weighted = False)["main_core"].vs["name"])
        counter +=1
        if counter % 5000 == 0:
 		print counter, 'core graphs have been extracted'
    return mcs_weighted, mcs_unweighted
    
def pageRank_keywords(graphs):
    PageRank = []
    counter = 0   
    for graph in graphs: 
        # compute PageRank scores 	
    	# hint: use .pagerank attribute of igraph objects
    	# store result as "pr_scores"
        pr_scores = graph.pagerank()
    	# preserve name one-to-one mapping before sorting
        pr_scores = zip(graph.vs["name"],pr_scores)	
        # rank in decreasing order
        pr_scores = sorted(pr_scores, key=operator.itemgetter(1), reverse=True)  
        # retain top 33% words as keywords
        numb_to_retain = int(round(len(pr_scores)/3))  
        keywords = [tuple[0] for tuple in pr_scores[:numb_to_retain]]  
        PageRank.append(keywords)      
        counter+=1
        if counter % 5000 == 0:
            print counter, "pageranks have been processed"      
    return PageRank

#%% Authors

    
"""
Some remarks about the messy data:
- \\c is the cédille
- \\"{u} puts an umlaut ü, and accent are often put that way (Claude Barrab\\`{e}s for example)

Broken strings:
 Przemys\l{aw} Siemion
 Dj. \v{S}ija\vc}ki
S\o{}ren K\"o{}ster

Problems:
Bj\o rn Jensen
"""
 

def special_cases(s):
    # Digits
    if s == r"T.A. Ioannidou, N.D. V1achos":
        return r"T.A. Ioannidou, N.D. Vlachos"
    if s == r"18 pages":
        return r""
    if s == r"Amitabha Lahiri, 10":
        return r"Amitabha Lahiri"
    if s == r"Ryuji KEMMOKU, Satoru SAITO, 13 pages, Latex":
        return r"Ryuji KEMMOKU, Satoru SAITO"
    if s == r"R. Banerjee, H.J. Rothe, K.D. Rothe Comments 9 pages, LaTeX":
        return r"R. Banerjee, H.J. Rothe, K.D. Rothe"
    # Commas instead of points
    if s == r"M. Cvetic, H, Lu, J.F. Vazquez-Poritz":
        return r"M. Cvetic, H. Lu, J.F. Vazquez-Poritz"
    if s == r"N. A. Ansari, L. Di Fiore, M.A.Man'ko, V, I, Man'ko, R. Romano, S.":
        return r"N. A. Ansari, L. Di Fiore, M.A.Man'ko, V. I. Man'ko, R. Romano"
    # Random initials
    if s == r"Emparan, R., Valle Basagoiti, M. A":
        return r"R. Emparan, M. A. Valle Basagoiti"
    if s == r"Celeghini, S.De Martino, S.De Siena, M.Rasetti, G.Vitiello":
        return r"E.Celeghini, S.De Martino, S.De Siena, M.Rasetti, G.Vitiello"
    if s == r"Bender, Pinsky, , van de S, e":
        return r"C. M. Bender, S. Pinsky, B. van de Sande"
    # Random }
    if s == r"Miao Li, Emil Martinec}":
        return r"Miao Li, Emil Martinec"
    # Slash instead of antislash
    if re.search(r'A. H. Chamseddine, J. Fr', s) is not None:
        return r'A. H. Chamseddine, J. Frohlich'
    # Fucking juniors
    s = re.sub(r'\, Jr\.', r'', s)
    s = re.sub(r'\, Jr', r'', s)
    s = re.sub(r'Jr\.', r'', s)
    s = re.sub(r'Jr', r'', s)
    s = re.sub(r'\, jr\.', r'', s)
    s = re.sub(r'\, jr', r'', s)
    s = re.sub(r'jr\.', r'', s)
    s = re.sub(r'jr', r'', s)
    s = re.sub(r'\, III\.', r'', s)
    s = re.sub(r'\, III', r'', s)
    s = re.sub(r'III\.', r'', s)
    s = re.sub(r'III', r'', s)
    s = re.sub(r'Taylor IV', r'Taylor', s)
    return s

def and_repairing(s):
    # Deal with the "and" -> ", " problem.
    if s == r'M. C. L, , N. Shnerb, , L. P. Horwitz':
        return r'M. C. Land, N. Shnerb, , L. P. Horwitz'
    if s == r'M. C. L, , L. P. Horwitz':
        return r'M. C. Land, L. P. Horwitz'
    # Cases such as "Hjelmeland," -> "Hjelmel, ," (terminal "and") are not fully caught at the moment.
    # This should not be a problem because ", ," is parsed as empty string.
    s = re.sub(r'\, ([a-z])', r'and\1', s)
    s = re.sub(r'Fern\\\'\, ', r'Fernand', s) # Maybe leave that for accent handling?
    # End of caption ands:
    l = [r'Bertr', r'Copel', r'DeGr', r'Devch', r'Dur', r'Ekstr', r'Goodb', \
         r'Megev', r'Nodl',  r'Noll',  r'Orl', r'Osl', \
         r'Pell', r'Rol', r'Strickl', r'Slingerl', r'Wendl']
    for prefix in l:
        s = re.sub(prefix+r'\,', prefix+r'and', s)
    # Special cases
    s = re.sub(r'Arm\, -Ugon', r'Armand-Ugon', s)
    s = re.sub(r'Arm\, Ugon', r'Armand-Ugon', s)
    return s
    
def parenthesis_removal(s):
    # Problem case: "Satish D. Joglekar, A. Misra ((1) Department of Physics, Indian"
    # We first remove the innermost parenthesis (and what's in it)
    # After that, only opening parentheses are left, we remove them and what's after.
    s =  re.sub(r'\(([^\(]*?)\)', '', s)
    if re.search(r'\(', s) is not None:
        s = s.split("(")[0]
    return s
    
def accent_removal(s):
    # Weird accents + missing end
    if s == r"H. Garc\' \I A-Compe\' an, J.M. L\' opez-Romero, M.A. Rodr\' \I":
        return r"H. Garcia-Compean, J.M. Lopez-Romero, M.A. Rodriguez"
    # Special/broken cases
    s = re.sub(r"\\Mary", r"Mary", s)
    s = re.sub(r"Bj\\o rn", r"Bjorn", s)
    s = re.sub(r"St\\a\'ephane", r"Stephane", s)
    s = re.sub(r"\\v{S}ija\\vc}ki", r"Sijacki", s)
    # Generic accents
    s = re.sub(r"\\o{}", r"o", s)
    s = re.sub(r"\\\"o{}", r"o", s)
    s = re.sub(r"\\'\\i ", r"i", s)
    s = re.sub(r"\\'\\i", r"i", s)
    s = re.sub(r"\\l{aw}", r"law", s)
    s = re.sub(r"\\ss{}", r"ss", s)
    s = re.sub(r'\\aa ', r'a', s)
    s = re.sub(r'{\\([a-z,A-Z]){2}}', r'\1', s) # ex: {\aa}
    s = re.sub(r'\\.{\\([a-z,A-Z])}', r'\1', s) # ex: \'{\e}
    s = re.sub(r'{\\\W([a-z,A-Z])}', r'\1', s) # ex: {\'e}
    s = re.sub(r'\\.{([a-z,A-Z])}', r'\1', s) # ex: \'{e}
    s = re.sub(r'\\\W([a-z,A-Z])', r'\1', s) # ex: {\'e}
    s = re.sub(r'{\\([a-z,A-Z])}', r'\1', s) # ex: {\e}
    s = re.sub(r'\\\W([a-z,A-Z])', r'\1', s) # ex: \'e
    s = re.sub(r'\\. ([a-z,A-Z])', r'\1', s) # ex: \v c (č)
    s = re.sub(r'"([a-z])', r'\1', s) # ex: M"uller
    s = re.sub(r'`', r'', s) # ex: Fre`
    return s

def parse_authors(author_captions):
    authors_by_paper = []
    count_bla = 0
    for paper_authors in author_captions:
        s = special_cases(paper_authors)
        s = and_repairing(s)
        s = parenthesis_removal(s)
        s = accent_removal(s)
        s = re.sub(r'~', r' ', s) # Espace insécable
        s = re.sub(r'--', r'-', s)
        s = re.sub(r'_', r'-', s) # cf. Chang_Pu Sun
        # Wrong separators
        s = re.sub(r';', r',', s)
        s = re.sub(r'&', r',', s)
        author_set = s.split(",")
        trimmed_set = []
        for i, author in enumerate(author_set):
            trimmed = author.strip()
            if trimmed != '':
                if re.search(r'\w\w', trimmed) is None:
                    count_bla += 1
                 # Don't use last name when pure initials or single word
                if (i != len(author_set)-1) or \
                ((re.search(r'\w\w', trimmed) is not None) \
                  and not (re.search(r' ', trimmed) is None \
                             and re.search(r'\.', trimmed) is None)):
                    trimmed = re.sub(r'\.-', r'-', trimmed)
                    trimmed = re.sub(r'\.', r' ', trimmed)
                    trimmed = re.sub(r'-', r' ', trimmed)
                    trimmed = re.sub(r'  ', r' ', trimmed)
                    trimmed = trimmed.lower().strip()
                    trimmed_set.append(trimmed)
        authors_by_paper.append(trimmed_set)
    return authors_by_paper
    
    
def shrink_authors(authors_by_paper):
    shrunk_by_paper = []
    for author_list in authors_by_paper:
        shrunk_list = []
        for author in author_list:
            last = author.split()[-1]
            initial = author[0]
            shrunk = initial+r' '+last
            shrunk_list.append(shrunk)
        shrunk_by_paper.append(shrunk_list)
    return shrunk_by_paper
    
def get_papers_by_author(info_by_ID):
    papers_by_author = {}
    for ID in info_by_ID.iterkeys():
        for author in info_by_ID[ID]["authors"]:
            if not papers_by_author.has_key(author):
                papers_by_author[author] = []
            papers_by_author[author].append(ID)
    return papers_by_author


#%% Edges

def adjacency_lists(training_edges):
    edges_from = {}
    edges_to = {}
    for src, dst, exists in training_edges:
        if exists:
            if not edges_from.has_key(src):
                edges_from[src] = []
            edges_from[src].append(dst)
            if not edges_to.has_key(dst):
                edges_to[dst] = []
            edges_to[dst].append(src)
    return edges_from, edges_to
    
def random_edges(IDs, edges_from, edges_to, n):
    np.random.shuffle(IDs)
    for k in xrange(n/len(IDs)*2):
        for i in xrange(len(IDs)/2):
            src = IDs[2*i]
            dst = IDs[2*i+1]
            if not edges_from.has_key(src):
                edges_from[src] = []
            edges_from[src].append(dst)
            if not edges_to.has_key(dst):
                edges_to[dst] = []
            edges_to[dst].append(src)
        
    
#%% Paper features:
    
def dictionnarize(node_info):
    info_by_ID = {}
    for i in xrange(len(node_info)):
        entry = {}
        entry["year"] = int(node_info[i][1])
        entry["title"] = node_info[i][2]
        entry["author_caption"] = node_info[i][3]
        entry["journal"] = node_info[i][4]
        entry["abstract"] = node_info[i][5]
        info_by_ID[int(node_info[i][0])] = entry
    return info_by_ID
    
    
        
    
def get_paper_features(node_info, DG_paper):
    features = dictionnarize(node_info)
    IDs = [int(element[0]) for element in node_info]
    title_words_by_paper = title_features(node_info)
    abstract_words_by_paper = abstract_features(node_info)
    abstract_keywords_by_paper = abstract_keywords(node_info, "PageRank")
    author_captions = [element[3] for element in node_info]
    authors_by_paper = parse_authors(author_captions)
    shrunk_by_paper = shrink_authors(authors_by_paper)
    # Import gephi
    usecols = [0,3,4,5,6,7,8,9]
    gephi = np.loadtxt("node_gephi.csv", delimiter=',', skiprows=1, usecols=usecols)
    for ID, title_words, abstract_words, abstract_keywordss, \
        author_list, shrunk_list in \
        zip(IDs, title_words_by_paper, abstract_words_by_paper, \
        abstract_keywords_by_paper, authors_by_paper, shrunk_by_paper):
        features[ID]["title_words"] = title_words
        features[ID]["abstract_words"] = abstract_words
        features[ID]["abstract_keywords"] = abstract_keywordss
        features[ID]["authors"] = set(author_list)
        features[ID]["shrunk_authors"] = set(shrunk_list)
        features[ID]["edges_from"] = [edge[1] for edge in DG_paper.out_edges(ID)]
        features[ID]["edges_to"] = [edge[0] for edge in DG_paper.in_edges(ID)]
    for (ID, modularity_class, pagerank, eccentricity, \
        closnesscentrality, harmonicclosnesscentrality, \
        betweenesscentrality, eigencentrality) in gephi:
        features[ID]["modularity_class"] = modularity_class
        features[ID]["pagerank"] = pagerank
        features[ID]["eccentricity"] = eccentricity
        features[ID]["closness_centrality"] = closnesscentrality
        features[ID]["harmonic_closnesscentrality"] = harmonicclosnesscentrality
        features[ID]["betweeness_centrality"] = betweenesscentrality
        features[ID]["eigen_centrality"] = eigencentrality
    for ID in set.difference(set(IDs), set(gephi[:,0])):
        features[ID]["modularity_class"] = 0
        features[ID]["pagerank"] = 0
        features[ID]["eccentricity"] = 0
        features[ID]["closness_centrality"] = 0
        features[ID]["harmonic_closnesscentrality"] = 0
        features[ID]["betweeness_centrality"] = 0
        features[ID]["eigen_centrality"] = 0
    return features
    
#%% Author features
#    - paper list
#    - cited authors
#    - cited papers
#    - citing authors -> useless because symmetric with cited?
#    - citing papers -> useless because symmetric with cited?
#    - collaborators with number of collaborations
#    - number of citations
#    - mean citation/paper
#    - max citation/paper
    
# Must be called on the features of get_paper_features
def get_author_features(info_by_ID, DG_paper):
    features = {}
    # Initialise keys and add list of papers
    for ID in info_by_ID.iterkeys():
        for author in info_by_ID[ID]["shrunk_authors"]:
            if not features.has_key(author):
                features[author] = {}
                features[author]["papers"] = []
                features[author]["cited_papers"] = Counter()
#                features[author]["citing_papers"] = Counter()
                features[author]["collaborators"] = Counter()
            features[author]["papers"].append(ID)
            features[author]["cited_papers"].update(info_by_ID[ID]["edges_from"])
#            features[author]["citing_papers"].update(info_by_ID[ID]["edges_to"])
            features[author]["collaborators"].update(info_by_ID[ID]["shrunk_authors"])
    for author in features.iterkeys():
        features[author]["cited_authors"] = Counter()
        for cited in features[author]["cited_papers"].iterkeys():
            # Add the authors of the cited paper to the counter as many times
            # as the current author has cited them.
            n_citations = features[author]["cited_papers"][cited]
            counts = {author: n_citations 
                        for author in info_by_ID[cited]["shrunk_authors"]}
            features[author]["cited_authors"].update(counts)
        cited_counts = []
        citing_counts = []
        for paper in features[author]["papers"]:
            cited_counts.append(len(info_by_ID[paper]["edges_to"]))
            citing_counts.append(len(info_by_ID[paper]["edges_to"]))
        features[author]["max_cited"] = max(cited_counts)
        features[author]["mean_cited"] = np.mean(cited_counts)
        features[author]["sum_cited"] = np.sum(cited_counts)
        features[author]["max_citing"] = max(citing_counts)
        features[author]["mean_citing"] = np.mean(citing_counts)
        features[author]["sum_citing"] = np.sum(citing_counts)
    return features
        
def make_author_related_paper_features(paper_features, author_features):
    for paper in paper_features.iterkeys():
        if len(paper_features[paper]["shrunk_authors"]) > 0:
#            paper_features[paper]["MaxCitedNumber"] = max([author_features[auth]["mean_cited"] for auth in paper_features[paper]["authors"]])
            paper_features[paper]["MaxCitedNumber"] = max([author_features[auth]["sum_cited"] for auth in paper_features[paper]["shrunk_authors"]])
            paper_features[paper]["AverageCitedNumber"] = np.mean([author_features[auth]["mean_cited"] for auth in paper_features[paper]["shrunk_authors"]])
            paper_features[paper]["MaxReferenceNumber"] = max([author_features[auth]["sum_cited"] for auth in paper_features[paper]["shrunk_authors"]])
            paper_features[paper]["AverageReferenceNumber"] = np.mean([author_features[auth]["mean_cited"] for auth in paper_features[paper]["shrunk_authors"]])
        else:
#            paper_features[paper]["MaxCitedNumber"] = 0
            paper_features[paper]["MaxCitedNumber"] = 0
            paper_features[paper]["AverageCitedNumber"] = 0
            paper_features[paper]["MaxReferenceNumber"] = 0
            paper_features[paper]["AverageReferenceNumber"] = 0
    
    
#%% 
"""
Returns the number of authors from dst_authors in src_abstract. Do it only
for last names.
"""
def nb_authors_in_abstract(src_abstract_words, dst_authors):
    last_names_set = set([author.split()[-1] for author in dst_authors])
    return len(set.intersection(last_names_set, set(src_abstract_words)))
    
    
#%% Edge features:


# Word of caution:
# We must take great care not to "leak" information about the presence of an
# edge in the features. Doind so would make the cross validation wrong.
# For example, if we did not remove the edge from G_paper, all pairs that are
# an edge would have a shortest path length of one, which would be the only
# needed feature to determine whether they are linked or not. But this feature
# is possible only because we already know at feature engineering time that 
# there is an edge, which won't be the case in the test set!
def get_edge_features(node_info, training_edges, bonus_edges=None, testing_edges=None):
    # Build graph of papers
    DG_paper=nx.DiGraph()
    IDs = [int(element[0]) for element in node_info]
    for ID in IDs:
        DG_paper.add_node(ID)
    for src, dst, exists in training_edges:
        if exists:
            DG_paper.add_edge(src, dst)
    if bonus_edges is not None:
        for src, dst, exists in bonus_edges:
            if exists:
                DG_paper.add_edge(src, dst)
    G_paper = nx.Graph(DG_paper)

    
    paper_features = get_paper_features(node_info, DG_paper)
    author_features = get_author_features(paper_features, DG_paper)
    # TODO: make these independent of the current edge
    make_author_related_paper_features(paper_features, author_features)
    
    
    DG_author=nx.DiGraph()    
    for author in author_features.iterkeys():
        DG_author.add_node(author)
        for cited in author_features[author]["cited_authors"].iterkeys():
            DG_author.add_edge(author, cited)
    G_author = nx.Graph(DG_author)
    
    G_collab=nx.Graph()    
    for author in author_features.iterkeys():
        G_collab.add_node(author)
        for collab in author_features[author]["collaborators"].iterkeys():
            G_collab.add_edge(author, collab)
    
    n_features = 56
    Xtr = np.zeros((len(training_edges),n_features))
    if testing_edges is not None:
        Xte = np.zeros((len(testing_edges),n_features))
    
    def feature_row(X, i, src, dst, exists):
        """
        Closure that computes the features for one pair, we use it in both
        the training features and the testing features.
        """
        # First step: remove the current edge from all features.
        if exists:
            paper_features[src]["edges_from"].remove(dst)
            paper_features[dst]["edges_to"].remove(src)
            G_paper.remove_edge(src, dst)
            DG_paper.remove_edge(src, dst)
            for author in paper_features[src]["shrunk_authors"]:
                for cited in paper_features[dst]["shrunk_authors"]:
                    if author_features[author]["cited_authors"][cited] == 1:
                        try:
                            DG_author.remove_edge(author, cited)
                            if not DG_author.has_edge(cited, author):
                                G_author.remove_edge(author, cited)
                        except nx.NetworkXError:
                            print src, dst, author, cited
                            return
                author_features[author]["cited_authors"].update({auth:-1 for auth in paper_features[dst]["shrunk_authors"]})
    
        k = -1
        # Year-related
        k+=1; X[i][k] = paper_features[src]["year"] - paper_features[dst]["year"]
#        k+=1; X[i][k] = np.log(1+max(0, paper_features[src]["year"] - paper_features[dst]["year"]))
        k+=1; X[i][k] = paper_features[src]["year"]
        k+=1; X[i][k] = paper_features[dst]["year"]
        # Title-related
        k+=1; X[i][k] = len(set(paper_features[dst]["title_words"]).intersection(set(paper_features[src]["title_words"])))
        # Abstract-related
        k+=1; X[i][k] = len(set(paper_features[dst]["abstract_words"]).intersection(set(paper_features[src]["abstract_words"])))
        k+=1; X[i][k] = len(set(paper_features[dst]["abstract_keywords"]).intersection(set(paper_features[src]["abstract_keywords"])))
        k+=1; X[i][k] = nb_authors_in_abstract(paper_features[src]["abstract_words"], paper_features[dst]["authors"])
        # Journal-related
        k+=1; X[i][k] = (paper_features[src]["journal"] != "") & (paper_features[src]["journal"] == paper_features[dst]["journal"])
        # Graph-related
        k+=1; X[i][k] = len(set(paper_features[dst]["edges_from"]).intersection(set(paper_features[src]["edges_from"])))
        #k+=1; X[i][k] = len(set(paper_features[dst]["edges_from"]).intersection(set(paper_features[src]["edges_to"]))) (impossible if connected)
        k+=1; X[i][k] = len(set(paper_features[dst]["edges_to"]).intersection(set(paper_features[src]["edges_from"])))
        k+=1; X[i][k] = len(set(paper_features[dst]["edges_to"]).intersection(set(paper_features[src]["edges_to"])))
        k+=1; X[i][k] = DG_paper.out_degree(src)
        k+=1; X[i][k] = DG_paper.out_degree(dst)
        k+=1; X[i][k] = DG_paper.in_degree(src)
        k+=1; X[i][k] = DG_paper.in_degree(dst)
        k+=1; X[i][k] = pref_attachment(G_paper, src, dst)
        k+=1; X[i][k] = cosine(G_paper, src, dst)
        k+=1; X[i][k] = jaccard(G_paper, src, dst)
        k+=1; X[i][k] = shortest_path_length(G_paper, src, dst)
        k+=1; X[i][k] = shortest_path_length(DG_paper, src, dst)
        # Gephi
        k+=1; X[i][k] = paper_features[src]["modularity_class"] == paper_features[dst]["modularity_class"]
        k+=1; X[i][k] = paper_features[dst]["betweeness_centrality"] - paper_features[src]["betweeness_centrality"]
        k+=1; X[i][k] = paper_features[dst]["pagerank"]
        k+=1; X[i][k] = paper_features[dst]["betweeness_centrality"]
        k+=1; X[i][k] = paper_features[dst]["closness_centrality"]
        k+=1; X[i][k] = paper_features[dst]["betweeness_centrality"]
        k+=1; X[i][k] = paper_features[dst]["harmonic_closnesscentrality"]
        k+=1; X[i][k] = paper_features[dst]["eigen_centrality"]
        k+=1; X[i][k] = paper_features[dst]["eccentricity"]
        k+=1; X[i][k] = paper_features[src]["eccentricity"]
        
        # Author-related
        k+=1; X[i][k] = len(paper_features[dst]["authors"].intersection(paper_features[src]["authors"]))
        k+=1; X[i][k] = len(paper_features[dst]["shrunk_authors"].intersection(paper_features[src]["shrunk_authors"]))
        k+=1; X[i][k] = len(paper_features[src]["authors"])
        k+=1; X[i][k] = len(paper_features[dst]["authors"])
        if len(paper_features[dst]["shrunk_authors"]) > 0:
            k+=1; X[i][k] = max([author_features[auth]["sum_cited"] for auth in paper_features[dst]["shrunk_authors"]])
            k+=1; X[i][k] = max([author_features[auth]["mean_cited"] for auth in paper_features[dst]["shrunk_authors"]])
            
            # CS224W features
            if len(paper_features[src]["shrunk_authors"]) > 0:
                auth_jaccards = []
                auth_spl = []
                collab_jaccards = []
                collab_spl = []
                n_collaborations = []
                n_citations = []
                for auth in paper_features[src]["shrunk_authors"]:
                    for cited in paper_features[dst]["shrunk_authors"]:
                        auth_jaccards.append(jaccard(G_author, auth, cited))
                        auth_spl.append(shortest_path_length(G_author, auth, cited))
                        collab_jaccards.append(jaccard(G_collab, auth, cited))
                        collab_spl.append(shortest_path_length(G_collab, auth, cited))
                    n_collaborations = n_collaborations + [author_features[auth]["collaborators"][collab] for collab in paper_features[dst]["shrunk_authors"]]
                    n_citations = n_citations + [author_features[auth]["cited_authors"][cited] for cited in paper_features[dst]["shrunk_authors"]]
                
                k+=1; X[i][k] = np.max(auth_jaccards)
                k+=1; X[i][k] = np.mean(auth_jaccards) 
                k+=1; X[i][k] = np.min(auth_spl)
                k+=1; X[i][k] = np.mean(auth_spl) 
                k+=1; X[i][k] = np.max(collab_jaccards)
                k+=1; X[i][k] = np.mean(collab_jaccards) 
                k+=1; X[i][k] = np.min(collab_spl)
                k+=1; X[i][k] = np.mean(collab_spl) #30
                
                k+=1; X[i][k] = np.max(n_collaborations) # MaxACollaberateWithB
                k+=1; X[i][k] = np.mean(n_collaborations) # MeanACollaberateWithB
                k+=1; X[i][k] = np.max(n_citations) # MaxACiteB
                k+=1; X[i][k] = np.mean(n_citations) # MeanACiteB
        k+=1; X[i][k] = paper_features[src]["MaxCitedNumber"]
        k+=1; X[i][k] = paper_features[src]["AverageCitedNumber"]
        k+=1; X[i][k] = paper_features[src]["MaxReferenceNumber"]
        k+=1; X[i][k] = paper_features[src]["AverageReferenceNumber"]
        k+=1; X[i][k] = paper_features[dst]["MaxCitedNumber"]
        k+=1; X[i][k] = paper_features[dst]["AverageCitedNumber"]
        k+=1; X[i][k] = paper_features[dst]["MaxReferenceNumber"]
        k+=1; X[i][k] = paper_features[dst]["AverageReferenceNumber"]
        
        
        # Last step: Put the edge back in the features
        if exists:
            paper_features[src]["edges_from"].append(dst)
            paper_features[dst]["edges_to"].append(src)
            G_paper.add_edge(src, dst)
            DG_paper.add_edge(src, dst)
            for author in paper_features[src]["shrunk_authors"]:
                author_features[author]["cited_authors"].update(paper_features[dst]["shrunk_authors"])
                for cited in paper_features[dst]["shrunk_authors"]:
                    if author_features[author]["cited_authors"][cited] == 1:
                        G_author.add_edge(author, cited)
                        DG_author.add_edge(author, cited)
    
    for i, (src, dst, exists) in enumerate(training_edges):
        if np.mod(i,1000) == 0:
            sys.stdout.write("\rTrain feature engineering advancement: %d%%" 
                %(100*float(i)/len(training_edges)))
            sys.stdout.flush()
        feature_row(Xtr, i, src, dst, exists)
    
    if testing_edges is None:
        return Xtr
    else:
        for i, (src, dst) in enumerate(testing_edges):
            if np.mod(i,1000) == 0:
                sys.stdout.write("\rTest feature engineering advancement: %d%%" 
                    %(100*float(i)/len(testing_edges)))
                sys.stdout.flush()
            feature_row(Xte, i, src, dst, False)
        return Xtr, Xte



