# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 15:35:08 2016

@author: gurvan
"""

import networkx as nx
import csv
import numpy as np
import matplotlib.pyplot as plt
import random
import scipy.sparse as sparse
import numpy as np
import numpy.linalg

from util import jaccard

DG=nx.DiGraph()

with open("node_information.csv", "r") as f:
    reader = csv.reader(f)
    node_info  = list(reader)

IDs = [int(element[0]) for element in node_info]
for ID in IDs:
    DG.add_node(ID)

training_edges = np.loadtxt("training_set.txt", dtype=int)
n_edges = len(training_edges)
n_train = 4*n_edges/5
for src, dst, exists in training_edges[:n_train]:
    if exists:
        DG.add_edge(src, dst)

G = nx.Graph(DG)

#%%

############## Question 2
# Network Characteristics
print 'Number of nodes:', G.number_of_nodes() 
print 'Number of edges:', G.number_of_edges() 
print 'Number of connected components:', nx.number_connected_components(G)

# Connected components
CC = list(nx.connected_component_subgraphs(G))
GCC = CC[0]

# Fraction of nodes and edges in GCC 
print "Fraction of nodes in GCC: ", GCC.number_of_nodes() / float(G.number_of_nodes())
print "Fraction of edges in GCC: ", GCC.number_of_edges() / float(G.number_of_edges())

#%%
############## Question 3
# Degree
degree_sequence = G.degree().values()
print "Min degree ", np.min(degree_sequence)
print "Max degree ", np.max(degree_sequence)
print "Median degree ", np.median(degree_sequence)
print "Mean degree ", np.mean(degree_sequence)

# Degree distribution
y=nx.degree_histogram(G)
plt.figure(1)
plt.plot(y,'b-',marker='o')
plt.ylabel("Frequency")
plt.xlabel("Degree")
plt.draw()
plt.show()
#f.savefig("degree.png",format="png")

plt.figure(2)
plt.loglog(y,'b-',marker='o')
plt.ylabel("Frequency")
plt.xlabel("Degree")
plt.draw()
plt.show()
#s.savefig("degree_loglog.png",format="png")


#%%
############## Question 4
# (I) Triangles
# Exact number of triangles using the built-in function
t=nx.triangles(GCC)
t_total = sum(t.values())/3
print "Total number of triangles ", t_total

# Distribution of triangle participation (similar to the degree distribution,
# i.e., in how many triangles each node participates to)
t_values = sorted(set(t.values()))
t_hist = [t.values().count(x) for x in t_values]
plt.figure(3)
plt.loglog(t_values, t_hist, 'bo')
plt.xlabel('Number of triangles')
plt.ylabel('Count')
plt.draw()
plt.show()
#plt.savefig("triangles.png",format="png")

# Average clustering coefficient
avg_clust_coef = nx.average_clustering(GCC)
print "Average clustering coefficient ", avg_clust_coef

#%%
############## Question 5
# Centrality measures

# Degree centrality
deg_centrality = nx.degree_centrality(G)

# Eigenvector centrality
eig_centrality = nx.eigenvector_centrality(G)

# Sort centrality values
sorted_deg_centrality = sorted(deg_centrality.items())
sorted_eig_centrality = sorted(eig_centrality.items())

# Extract centralities
deg_data=[b for a,b in sorted_deg_centrality]
eig_data=[b for a,b in sorted_eig_centrality]

# Compute Pearson correlation coefficient
from scipy.stats.stats import pearsonr
print "Pearson correlation coefficient ", pearsonr(deg_data, eig_data)


#%%############################################################################
# ANALYSIS THAT REQUIRE HOLDOUT SET
###############################################################################

DG=nx.DiGraph()

with open("node_information.csv", "r") as f:
    reader = csv.reader(f)
    node_info  = list(reader)

IDs = [int(element[0]) for element in node_info]
for ID in IDs:
    DG.add_node(ID)

n_train = 4*n_edges/5
for src, dst, exists in training_edges[:n_train]:
    if exists:
        DG.add_edge(src, dst)

G = nx.Graph(DG)


#%% Shortest paths

from collections import Counter

linked = Counter()
not_linked = Counter()
linked_no_path = 0
not_linked_no_path = 0
for src, dst, exists in training_edges[n_train:]:
    try:
        length = nx.shortest_path_length(G, src, dst)
        if exists:
            linked.update([nx.shortest_path_length(G, src, dst)])
        else:
            not_linked.update([nx.shortest_path_length(G, src, dst)])
        continue
    except nx.NetworkXNoPath:
        if exists:
            linked_no_path += 1
        else:
            not_linked_no_path += 1
        continue
         

plt.figure()
plt.title("Shortest path length between linked nodes")
plt.bar(linked.keys(), linked.values())
plt.xlim([1, 14])

plt.figure()
plt.title("Shortest path length between not linked nodes")
plt.bar(not_linked.keys(), not_linked.values())
plt.xlim([1, 14])

#%% Link-based Jaccard coefficient

linked_jaccard = []
not_linked_jaccard = []

for src, dst, exists in training_edges[n_train:]:
    src_neighbors = set(G.neighbors(src))
    dst_neighbors = set(G.neighbors(dst))
    if exists:
        linked_jaccard.append(jaccard(G, src, dst))
    else:
        not_linked_jaccard.append(jaccard(G, src, dst))


plt.figure()
plt.title("Jaccard between linked nodes")
plt.hist(linked_jaccard, log=True, bins=50)
plt.xlim([0, 1])
plt.show()    
        
plt.figure()
plt.title("Jaccard between not linked nodes")
plt.hist(not_linked_jaccard, log=True, bins=10)
plt.xlim([0, 1])
plt.show()
    
    
    
    
    
    