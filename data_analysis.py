# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 12:01:26 2016

@author: gurvan
"""



"""

Remarks on datset:
- there seems to be only two categories: theoretical physics and theoretical math
- I did not understand the year difference histogram

Possible features for a simple model:
- number of overlapping words in title
- number of shared authors
- number of authors of each
- difference in years
- try functions of year difference
- number of shared citations
- number of shared references
- number of citations of each
- number of references of each
- same journal (bool)
- number of papers from authors of each
- number of citations for authors of each


==> Throw this in Random Forest, ExtraTrees, GradBoost, logreg, SVM...

More experimental features:
- length of abstract
-> difference of length
- number of digits in abstract
- number of overlapping complex words in abstract (length? prob through nltk?)
- longest common substring
- overlapping single-letter words and digits, hyphen as spaces (capture things like R, Z, C, k)
- retrieve university from author caption

"""




import numpy as np
import csv
import matplotlib.pyplot as plt
import re
import networkx as nx

from collections import Counter

from feature_engineering import dictionnarize


training_edges = np.loadtxt("training_set.txt", dtype=int)

with open("node_information.csv", "r") as f:
    reader = csv.reader(f)
    node_info  = list(reader)
    
info_by_ID = dictionnarize(node_info)
    
#%% Edge proportions
    
print "The training pairs contain %s links and %s non-links." \
    %(np.sum(training_edges[:,2] == 1), np.sum(training_edges[:,2] == 0))
    
nodes = set(np.unique(training_edges[:,:2]))
IDs = set([int(element[0]) for element in node_info])
# We have nodes == IDs

    
#%% Degree histograms
    
DG=nx.DiGraph()
for ID in IDs:
    DG.add_node(ID)
for src, dst, exists in training_edges:
    if exists:
        DG.add_edge(src, dst)
# G = DG.to_undirected() # deep copy
G = nx.Graph(DG) # shallow copy  
    

in_degree = {}
out_degree = {}
for node in nodes:
    in_degree[node] = len(DG.in_edges(node))
    out_degree[node] = len(DG.out_edges(node))

#src cites dst:
#- dst is in edges_from[src]  
#- src is in edges_to[dst]    
    
from feature_engineering import adjacency_lists
    
edges_from, edges_to = adjacency_lists(training_edges)

in_degree = {}
out_degree = {}
for ID in IDs:
    if edges_to.has_key(ID):
        in_degree[ID] = len(edges_to[ID])
    else:
        in_degree[ID] = 0
    if edges_from.has_key(ID):
        out_degree[ID] = len(edges_from[ID])
    else:
        out_degree[ID] = 0
    
in_values = np.array(in_degree.values())
out_values = np.array(out_degree.values())

plt.figure()
plt.title("Out-degree distribution")
plt.hist(out_values[out_values < 100], bins=50)
plt.figure()
plt.title("In-degree distribution")
plt.hist(in_values[in_values < 100], bins=50)

# The out degree distribution is more fat-tailed than out degree, as expected.

#%% Years

years = [int(element[1]) for element in node_info]
plt.figure()
plt.title("Year distribution")
plt.hist(years)


year_diff = []
for src, dst, connected in training_edges:
    if connected:
        year_diff.append(info_by_ID[src]["year"] - info_by_ID[dst]["year"])

plt.figure()
plt.title("Year difference distribution in connected")
plt.hist(year_diff, bins=21)

year_diff_disconnected = []
for src, dst, connected in training_edges:
    if not connected:
        year_diff_disconnected.append(info_by_ID[src]["year"] - info_by_ID[dst]["year"])

plt.figure()
plt.title("Year difference distribution in disconnected")
plt.hist(year_diff_disconnected, bins=22)




#%% Authors
from feature_engineering import parse_authors



# Unsolved problems:
# - cut captions: F. Finkel, D. Gomez-Ullate, A. Gonzalez-Lopez, M.A. Rodriguez, R.
#       -> Zhadnov is missing, cf. Google.
#       -> We end up with authors such as "R.", "Par", "M.G.A."
#   -> For now, discard those that do not contain a point or a space as they as cut.
# TODO: Observe the ending names that appear only once
#
# - Mueller / Müller, two different writings for a same name (H.J.W. Mueller-Kirsten)
#
# 

#About initials:
#    C.M. Viallet
#    CM. Viallet
#    C. Viallet
#    C-M. Viallet
#    C.-M. Viallet


# DESTRUCTIVE DECISIONS
# - discard last author when only made of single letter words (cant do anything with it anyway)
# - discard last author when single word (cant do anything with it anyway)
# - /!\/!\/!\ shrink non-family names into their first letter /!\/!\/!\



author_captions = [element[3] for element in node_info]

authors_by_paper = parse_authors(author_captions)

all_authors = set()
author_counts = Counter()
total_count = 0
for author_list in authors_by_paper:
    total_count += len(author_list)
    author_counts.update(author_list)
    for author in author_list:
        all_authors.add(author)

multi_authors = 0
for author in author_counts.iterkeys():
    if author_counts[author] > 1:
        multi_authors += 1
        
last_names = {}
for author in all_authors:
    last = author.split()[-1]
    if not last_names.has_key(last):
        last_names[last] = []
    last_names[last].append(author)

shrunk_names = {}
for author_list in authors_by_paper:
    for author in author_list:
        last = author.split()[-1]
        initial = author[0]
        shrunk = initial+r' '+last
        if not shrunk_names.has_key(shrunk):
            shrunk_names[shrunk] = 0
        shrunk_names[shrunk] += 1

multi_shrunk_authors = 0
for author in shrunk_names.iterkeys():
    if shrunk_names[author] > 1:
        multi_shrunk_authors += 1

        
print "There are", total_count, "non-unique authors in the", len(node_info), \
    "papers, reduced to", len(all_authors), "unique authors and", \
    multi_authors, "authors that have more than one paper."
        
print "When shrinking the non-family names into their first letter (DESTRUCTIVE), there are only", \
     len(shrunk_names), "unique authors and", \
    multi_shrunk_authors, "authors that have more than one paper."
    
    
    
#%% Retrieving universities
    
# TODO: Capture university in author caption where possible

# Demonstration: "Stanford", "ENS", "MIT", "M.I.T", "New York"...   
#count = 0
#for cap in author_captions:
#    if re.search(r"Polytechnique", cap) is not None:
#        count += 1
#        print cap
#print count


insides = []
for cap in author_captions:
    if re.search(r"\([A-Z]", cap) is not None:
        if re.search(r"\)", cap):
            insides.append(cap[cap.index("(") + 1:cap.index(")")])
        else:
            insides.append(cap[cap.index("(") + 1:])

    

#%% Titles

titles = [element[2] for element in node_info]

print "There are only %s unique titles out of %s articles!!!" \
    %(len(np.unique(titles)), len(titles))

title_count = Counter(titles)
multiple = []
for key in title_count.iterkeys():
    if title_count[key] > 1:
        multiple.append(key)
        
title_count = Counter(titles)
multiple_titles = set()
for key in title_count.iterkeys():
    if title_count[key] > 1:
        print title_count[key], key
        multiple_titles.add(key)
        
multiple_title_map = {}
for i in xrange(len(node_info)):
    if node_info[i][2] in multiple_titles:
        if not multiple_title_map.has_key(node_info[i][2]):
            multiple_title_map[node_info[i][2]] = []
        multiple_title_map[node_info[i][2]].append(node_info[i])
        


#%% Journals

journals = [element[4] for element in node_info]

c = Counter(journals)
# Many useless "journals"
# A few messy journals ("the", "rejected"...) but many of high interest



#%% IDs

plt.hist(IDs)


far_connected = 0
far_not_connected = 0
close_connected = 0
close_not_connected = 0


for src, dst, connected in training_edges:
    if connected:
        if np.abs(src - dst) < 5e6:
            close_connected += 1
        else:
            far_connected += 1
    else:
        if np.abs(src - dst) < 5e6:
            close_not_connected += 1
        else:
            far_not_connected += 1
            
print 100*236670/float(236670+98460), "% of linked pairs are in the same cluster."

small_year = Counter()
big_year = Counter()

for ID in IDs:
    if np.abs(ID) < 5e6:
        small_year.update([info_by_ID[ID]["year"]])
    else:
        big_year.update([info_by_ID[ID]["year"]])

# Considering the counters, it seems that there was a change of ID system
# 2003, therefore the ID clusters are akin to a binary variable stating whether
# the paper was registered before or after that time. This does not add much
# information to the publishing year data.


#%% Author graph
import networkx as nx
from feature_engineering import get_paper_features, get_author_features, get_papers_by_author

DG=nx.DiGraph()
IDs = [int(element[0]) for element in node_info]
for ID in IDs:
    DG.add_node(ID)
for src, dst, exists in training_edges:
    if exists:
        DG.add_edge(src, dst)
# G = DG.to_undirected() # deep copy
G = nx.Graph(DG) # shallow copy    


info_by_ID = get_paper_features(node_info, DG)

papers_by_author = get_papers_by_author(info_by_ID)

paper_counts = []
for author in papers_by_author.iterkeys():
    paper_counts.append(len(papers_by_author[author]))


plt.figure()
plt.title("Number of papers by author distribution")
plt.hist(paper_counts, log=True,bins=30)

author_features = get_author_features(info_by_ID, DG)

DG_author=nx.DiGraph()    
for author in author_features.iterkeys():
    DG_author.add_node(author)
    for cited in author_features[author]["cited_authors"].iterkeys():
        DG_author.add_edge(author, cited)
G_author = nx.Graph(DG_author)


src = 208144
dst = 7142

for author in info_by_ID[src]["shrunk_authors"]:
    for cited in info_by_ID[dst]["shrunk_authors"]:
        if author_features[author]["cited_authors"][cited] == 1:
            DG_author.remove_edge(author, cited)
            G_author.remove_edge(author, cited)


#%% Abstacts

from feature_engineering import abstract_keywords

keywords = abstract_keywords(node_info)



#%% Gephi
usecols = [0,3,4,5,6,7,8,9]
gephi = np.loadtxt("node_gephi.csv", delimiter=',', skiprows=1, usecols=usecols)

cols = ["ID", "modularity_class", "pageranks", "eccentricity", \
        "closnesscentrality", "harmonicclosnesscentrality", \
        "betweenesscentrality", "eigencentrality"]


plt.figure()
plt.title("Number of papers by "+cols[1])
plt.hist(gephi[:,1] ,bins=30)

plt.figure()
plt.title("Number of papers by "+cols[2])
plt.hist(gephi[:,2] ,log=True, bins=30)

plt.figure()
plt.title("Number of papers by "+cols[3])
plt.hist(gephi[:,3] ,bins=30)

plt.figure()
plt.title("Number of papers by "+cols[4])
plt.hist(gephi[:,4] ,bins=30)

plt.figure()
plt.title("Number of papers by "+cols[5])
plt.hist(gephi[:,5] ,bins=30)

plt.figure()
plt.title("Number of papers by "+cols[6])
plt.hist(gephi[:,6], log=True, bins=30)

plt.figure()
plt.title("Number of papers by "+cols[7])
plt.hist(gephi[:,7], log=True, bins=30)